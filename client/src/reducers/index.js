import { combineReducers } from 'redux';
import usuarioReducer from './usuarioReducer';
import procesosReducer from './procesosReducer';
//import productosReducer from './productosReducer';
import alertaReducer from './alertaReducer';

// export default combineReducers({
//     usuario: usuarioReducer,
//     productos: productosReducer,
//     alerta: alertaReducer
// });

export default combineReducers({
    tiendas: procesosReducer,
    usuario: usuarioReducer,
    alerta: alertaReducer
});