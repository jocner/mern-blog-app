const mongoose = require('mongoose');

const TiendaASchema = mongoose.Schema({
   
   usuario: {
     type: mongoose.Schema.Types.ObjectId, 
     ref: 'Usuario'
   },
   tienda: {
        type: String,
        require: false,
        trim: true
   },
   monto: {
        type: Number,
        require: true,
        trim: false
   },
   registro: {
       type: Date,
       default: Date.now()
   }
});

module.exports = mongoose.model('TiendaA', TiendaASchema);