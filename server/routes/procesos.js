//Rutas para crear usuarios 
const express = require('express');
const router = express.Router();
const procesoController = require('../controllers/procesosController');
//const auth = require('../middleware/auth');
const { check } = require('express-validator');

//Crea un proceso
// api/agregar
router.post('/', 
  
    [
        
        
        check('tienda', 'La tienda es obligatoria').not().isEmpty(),
        check('monto', 'El monto es obligatorio').not().isEmpty()
    ],
    procesoController.postAgregar
);

//Descontar un proceso
//api/descontar
// router.post('/', 
//     [
        
//         check('email', 'El email es obligatorio').isEmail(),
//         check('tienda', 'La tienda es obligatoria').not().isEmpty(),
//         check('monto', 'El monto es obligatorio').isNumeric()
//     ],
//     procesoController.postDescontar
// );

module.exports = router;